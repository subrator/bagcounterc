#ifndef APP_CONFIG
#define APP_CONFIG

extern char plugin_device[65];
extern char plugin_name[65];
extern char model_file[129];
extern char label_file[129];
extern char destination[65];
extern char device_id[65];
extern char resource_files[129];
extern long roi_x1;
extern long roi_y1;
extern long roi_x2;
extern long roi_y2;
extern long callback_frequency;
extern double threshold_score;
extern bool loadConfiguration();

#endif
