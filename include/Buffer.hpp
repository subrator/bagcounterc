/*

Copyright (c) 2020, CIMCON Software India Pvt. Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef BUFFER_C
#define BUFFER_C

#include "Defines.hpp"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct buffer_t buffer_t;

extern buffer_t* buffer_allocate(const void* data, size_t sz);
extern buffer_t* buffer_allocate_default();

extern buffer_t* buffer_copy(buffer_t* dest, buffer_t* orig);
extern buffer_t* buffer_append(buffer_t* dest, const void* data, size_t sz);
extern buffer_t* buffer_append_string(buffer_t* dest, const char* data);
extern buffer_t* buffer_append_integer(buffer_t* dest, const long data);
extern buffer_t* buffer_append_real(buffer_t* dest, const double data);
extern buffer_t* buffer_append_real_scientific(buffer_t* dest, const double data);

extern buffer_t* buffer_append_char(buffer_t* dest, const char data);
extern buffer_t* buffer_append_boolean(buffer_t* dest, const bool data);
extern buffer_t* buffer_append_iso_timestamp(buffer_t* dest);
extern buffer_t* buffer_append_unix_timestamp(buffer_t* dest);
extern buffer_t* buffer_append_unix_timestamp_ms(buffer_t* dest);

extern void buffer_remove(buffer_t* ptr, size_t start, size_t len);
extern void buffer_remove_end(buffer_t* ptr, size_t len);
extern void buffer_remove_start(buffer_t* ptr, size_t len);

extern void buffer_clear(buffer_t* ptr);
extern void buffer_free(buffer_t* ptr);

extern bool buffer_is_equal(buffer_t* first, buffer_t* second);
extern bool buffer_is_greater(buffer_t* first, buffer_t* second);
extern bool buffer_is_less(buffer_t* first, buffer_t* second);
extern bool buffer_is_null(buffer_t* ptr);

extern const void* buffer_get_data(buffer_t* ptr);
extern size_t buffer_get_size(buffer_t* ptr);

#ifdef __cplusplus
}
#endif

#endif
